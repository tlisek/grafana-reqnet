from urllib.request import urlopen
import json

import influxdb_client, os, time
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

REQNET_IP = "192.168.18.31"

token = "dW4VKBlZ-UcZbWJALH7mwp1qTHiDseW5bUUssSlWYitRGm9ctQ5K1sURuI3XEodKi-dRqUFme_f-oN1GBzZ2DQ=="
org = "home"
url = "http://influxdb:8086"

def getParams():

	url = "http://"+REQNET_IP+"/API/RunFunction?name=CurrentWorkParameters"
	key_map = {
		3: 'nawiew',
		4: 'wyciag',
		7: 'wilgotnosc',
		8: 'co2',
		39: 'bypass',
		63: 'opor_nawiew',
		64: 'opor_wyciag',
		65: 'went_nawiew',
		66: 'went_wyciag',
		67: 't_komf',
		81: 'moc_went_nawiew',
		82: 'moc_went_wyciag',
	}
	
	values = {}
		
	response = urlopen(url)
	if not response:
		return {}
	
	response_json = json.loads(response.read())
	if not response_json:
		return {}
		
	for idx in key_map:
		key = key_map[idx]
		val = response_json['Values'][idx]
		values[key] = val
			
	return values

def getTemps():

	url = "http://"+REQNET_IP+"/API/RunFunction?name=GetTemperatures"
	key_map = {
		'Extract': 't_wyciag',
		'Intake': 't_czerpnia',
		'Launcher': 't_wyrzutnia',
		'Supply': 't_nawiew',
	}
	
	values = {}
		
	response = urlopen(url)
	if not response:
		return {}
	
	response_json = json.loads(response.read())
	if not response_json:
		return {}
		
	for idx in key_map:
		key = key_map[idx]
		val = response_json[idx]
		values[key] = float(val)
			
	return values

def scrapeReku():

	params = getParams()
	temps = getTemps()

	all_values = params | temps

	# print(all_values)

	write_client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)
	write_api = write_client.write_api(write_options=SYNCHRONOUS)

	p =  Point("reku")

	for key in all_values:
		val = all_values[key]
		
		p = p.field(key, val)
		
	write_api.write(bucket="reku", org=org, record=p)
		