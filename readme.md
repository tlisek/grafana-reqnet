# Grafana - reQnet

## Instalacja
Konfiguracja stworzona w Portainer na Unraid.

Plan działania:
1. Utworzenie folderów `mnt/user/appdata/grafana` i `/mnt/user/appdata/influxdb`
2. Skopiowanie folderu `python` do `mnt/user/appdata/grafana/python`
3. Uruchomienie nowego Stacka z pliku `compose.yml`
4. Utworzenie tokena API w InfluxDB zgodnie z https://docs.influxdata.com/influxdb/cloud/admin/tokens/create-token/
5. Ustawienie parametrów do połaczenia z InfluxDB i reQnetem w `mnt/user/appdata/grafana/python/modules/scrapeReku.py`
6. Konfiguracja połączenia z InfluxDB w Grafanie
7. Import dashboardu do Grafany z `grafana/dashboard.json`
